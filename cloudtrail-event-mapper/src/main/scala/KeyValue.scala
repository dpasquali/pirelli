import play.api.libs.json.JsValue

case class KeyValue(key : String, value : JsValue) {

  def getJson() : String = {

    return s"""$key : "$value" """

  }

}
