import java.io.File

import play.api.libs.json._

object Mapper {

  def getType(jk : String, js : JsValue) : JsObject = {

    if(jk == "eventTime")
      return Json.obj("type" -> "date")

    val t = js match {
      case s: JsString => "keyword"
      case b: JsBoolean => "boolean"
      case o: JsObject => "keyword"
      case n: JsNumber => "double"
    }
    Json.obj("type" -> t)
  }

  def mapping(js: JsValue) : JsObject = js.as[JsObject].fields.foldLeft(Json.obj()) {

    case (acc, (k, v: JsObject)) => {
      var nj = Json.obj()
      nj += (k -> Json.obj("properties" -> mapping(v)))
      acc.deepMerge(nj)
    }
    case (acc, (k, v: JsArray)) => {

      var nja = Json.obj()
      val body = Json.obj("type" -> "nested")
        .deepMerge(Json.obj("properties" -> mappingArray(v).foldLeft(Json.obj())(_++_)))

      mappingArray(v).foldLeft(Json.obj())(_++_)
      nja += (k -> body)
      acc.deepMerge(nja)
    }
    case (acc, (k, JsNull)) => {
      acc + (k -> Json.obj("properties" -> Json.obj()))
    }
    case (acc, (k, v)) => {
      acc + (k -> getType(k,v))
    }
  }

  def mappingSeq(s: Seq[Any], b: Seq[JsObject] = Seq()): Seq[JsObject] = {
    s.foldLeft[Seq[JsObject]](b){
      case (acc, v: JsObject) =>
        acc:+v
      case (acc, v: Seq[Any]) =>
        mappingSeq(v, acc)
    }
  }

  def mappingArray(a: JsArray): Seq[JsObject] = {
    mappingSeq(a.value.zipWithIndex.map {
      case (o: JsObject, i: Int) =>
        mapping(o)
      case (o: JsArray, i: Int) =>
        mappingArray(o)
      case a =>
//        Json.obj("type" -> "nested").deepMerge(Json.obj("properties" -> Json.obj()))
      Json.obj()
    })
  }

  def mappingFirstLevel(js : JsValue) : JsObject = {

    var root = Json.obj()
    root.deepMerge(Json.obj("mappings" -> Json.obj("log" -> Json.obj("properties" -> mapping(js)))))
  }

  def mergeMappings(oldM: JsObject, newM : JsObject) : JsObject = {

//    val s1 = """{"a":10,"b":{},"d":[{"v":1},{"v":2}]}"""
//    val s2 = """{"a":10,"b":{"c":"ciao"}}"""
//
//    val j1 = Json.parse(s1).as[JsObject]
//    val j2 = Json.parse(s2).as[JsObject]
//
//    println(j1.toString())
//    println(j2.toString())
//
//    val jMerge = j1.deepMerge(j2)
//
//    println(jMerge)

    oldM.deepMerge(newM)
  }

}
