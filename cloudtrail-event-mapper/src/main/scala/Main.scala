import java.io.{File, PrintWriter}

import org.rogach.scallop.ScallopConf
import play.api.libs.json._

import scala.io.Source
import scalaj.http.{Http, HttpOptions, HttpResponse}

object Main {

  //var ELASTIC_HOST_PORT = "localhost:9200"

  def postMapping(hostport : String, mapping : String, index : String) : HttpResponse[String] = {

    Http(s"http://${hostport}/${index}").put(mapping)
      .header("Content-Type", "application/json")
      .header("Charset", "UTF-8")
      .option(HttpOptions.readTimeout(10000)).asString
  }

  class Conf(arguments: Seq[String]) extends ScallopConf(arguments) {

    val mappingDir = opt[String](required = true)

    val map = opt[Boolean]()
    val source = opt[String]()
    dependsOnAll(map, List(source, mappingDir))

    val post = opt[Boolean]()
    val elasticHost = opt[String](default = Some("localhost:9200"))
    dependsOnAll(post, List(elasticHost, mappingDir))

    val bulk = opt[Boolean]()
    val bulkDir = opt[String]()
    dependsOnAll(bulk, List(source, bulkDir))

    mutuallyExclusive(map, post)
    verify()
  }

  def bulkRequest(conf : Conf): Unit = {

    println("Make a Bulk")

    var bulkDir = conf.bulkDir()
    val filepath = conf.source()
    var filename = filepath.split("/").last
    var bulkWriter = new PrintWriter(new File(s"$bulkDir/bulk_$filename"))

    val bufferedSource = Source.fromFile(filepath)

    var eventName = ""
    var eventSource = ""

    for (line <- bufferedSource.getLines) {

      val jev = Json.parse(line)
      eventName = (jev \ "eventName").as[String].toLowerCase
      eventSource = (jev \ "eventSource").as[String].toLowerCase

      val bulkIndex = Json.obj("index" -> Json.obj("_index" -> s"$eventSource.$eventName").deepMerge(Json.obj("_type" -> "log")))

      bulkWriter.append(bulkIndex.toString()+"\n")
      bulkWriter.append(line+"\n")
    }

    bulkWriter.append("\n")
    bufferedSource.close
    bulkWriter.close()

  }

  def mappingRequest(conf : Conf): Unit = {

    val mappingDir = conf.mappingDir()

    println("Let's Map!")

    val filepath = conf.source()
    //var filepath = "test/split_big.json"

    var filename = filepath.split("/").last

    val bufferedSource = Source.fromFile(filepath)

    var eventName = ""
    var eventSource = ""

    for (line <- bufferedSource.getLines) {

      val jev = Json.parse(line)
      eventName = (jev \ "eventName").as[String].toLowerCase
      eventSource = (jev \ "eventSource").as[String].toLowerCase

      val mapping = Mapper.mappingFirstLevel(jev)

      if(!new File(s"$mappingDir/$eventName.$eventSource.json").exists){

        //println("new")

        var pw = new PrintWriter(new File(s"$mappingDir/$eventName.$eventSource.json"))
        pw.write(mapping.toString())
        pw.close()
      }
      else{
        val old = Source
          .fromFile(new File(s"$mappingDir/$eventName.$eventSource.json"))
          .mkString

        val legacy = Json.parse(old).as[JsObject]

        if(!legacy.equals(mapping)){
          //println("merge")
          var pw = new PrintWriter(new File(s"$mappingDir/$eventName.$eventSource.json"))
          pw.write(Mapper.mergeMappings(legacy, mapping).toString())
          pw.close()
        }
        //else println("equals")
      }
    }
    bufferedSource.close
  }

  def indexRequest(conf : Conf): Unit = {

    val mappingDir = conf.mappingDir()

    println("Post to ELK")

    var hostport = conf.elasticHost()
    val files = new File(mappingDir).listFiles().filter(_.isFile).toList

    for(f <- files) {

      val index = f.getName.replace(".json", "")

      val mapping = Source.fromFile(f).mkString
      var result: HttpResponse[String] =
        postMapping(hostport, mapping, index)

      if (result.code != 200 && result.code != 201) {
        println(s"mapping fail => ${index}")
        println(result.body)
        println()
      }
    }
  }

  def main(args: Array[String]): Unit = {

    val conf = new Conf(args)

    if(conf.bulk.isSupplied){
      bulkRequest(conf)
    }

    if(conf.map.isSupplied){
      mappingRequest(conf)
    }

    if(conf.post.isSupplied){
      indexRequest(conf)
    }

  }
}
