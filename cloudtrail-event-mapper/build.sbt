name := "JSONCloudtrailMapper"

version := "0.1"

scalaVersion := "2.11.6"

libraryDependencies += "com.typesafe.play" %% "play-json" % "2.6.8"
libraryDependencies += "org.scalaj" % "scalaj-http_2.11" % "2.3.0"
libraryDependencies += "org.rogach" %% "scallop" % "3.1.1"