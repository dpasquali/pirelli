import boto3
from datetime import datetime
import sys
import os
import requests
import subprocess
import io
import logging

LOG_LEVEL = logging.DEBUG

logs_folder = "logs"
mappings_folder = "mappings"
bulk_folder = "bulk"
failed_file_name = "failed.log"
split_folder = "split"

#elastic_host_port = "elasticsearch-log.pirelli.com"
#elastic_host_port = "10.203.135.168"
elastic_host_port = "vpc-cloudtrail-log-analysis-mw6nxceelz7zgwfhnn6ij7gugq.eu-west-1.es.amazonaws.com"

index_suffix = ".amazonaws.com"

proxyDict = { 
          'http'  : '', 
          'https' : ''
        }

mm = dict()
mm["01"] = 31
mm["02"] = 28
mm["03"] = 31
mm["04"] = 30
mm["05"] = 31
mm["06"] = 30
mm["07"] = 31
mm["08"] = 31
mm["09"] = 30
mm["10"] = 31
mm["11"] = 30
mm["12"] = 31


def subprocess_run(command):
    
    logging.info("Run bash command")
    logging.info(command)

    p = subprocess.Popen(command,
        shell=True,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT)
    out, err = p.communicate(b"' stdin")
    cd = p.returncode

    logging.info("command return code " + str(cd))

    return out, err, cd

def merge_bulks(source_folder, bulk_name):    

    with open(bulk_name, 'w') as merge:
        for bulk in os.listdir(source_folder):
            with open(source_folder+"/"+bulk, 'rb') as bulkfile:
                data = bulkfile.read()
                merge.write(data)
            subprocess_run("rm -rf "+source_folder+"/"+bulk)

def extract_year_logs(bucket, year):
    
    print("Estract Year ########################")
    print(year)
    print("###################################")
    
    
    logging.info("#### Estract year ####")
    logging.info(year)
    logging.info("###################")
    
    for m in range(1, 13):
        mm = ""
        if(m < 10):
            mm += "0"+str(m)
        else:
            mm = str(m)
        extract_month_logs(bucket, year, mm)
    
    #merge_bulks(montly_bulk_folder, "bulk_"+year+".json")


def extract_month_logs(bucket, year, month):
    
    print("Estract month ########################")
    print(year+"/"+month)
    print("###################################")

    logging.info("#### Estract month ####")
    logging.info(year+"/"+month)
    logging.info("###################")

    days = mm[month]

    logging.info("must process " + str(days) + " days")

    for d in range(1, days+1):
        dd = ""
        if(d < 10):
            dd += "0"+str(d)
        else:
            dd = str(d)
        extract_logs(bucket, year, month, dd)
    
    #merge_bulks(bulk_folder, "bulk_"+year+month+".json")
    #subprocess_run("mv " + "bulk_"+year+month+".json" + " " + montly_bulk_folder)


def extract_logs(bucket, year, month, day):
    print("Estract Day ########################")
    print(year+"/"+month+"/"+day)
    print("#################################")    

    logging.info("#### Estract day ####")
    logging.info(year+"/"+month+"/"+day)
    logging.info("###################")

    temp_file = bulk_folder+"/bulk_"+year+month+day+".json"

    logging.info("Temp file is "+temp_file)

    temp_file = split_folder+"/split_"+year+month+day+".json"

    for log in bucket.objects.filter(Prefix="AWSLogs/256909349812/CloudTrail/eu-west-1/"+year+"/"+month+"/"+day):
        key = log.key
        split = key.split("/")
        fn = logs_folder+"/"+ split[len(split)-1]

        logging.info("Read " + key + " and store to " + fn)

        body = log.get()['Body'].read()
        
        logging.info("Body size: " + str(len(body)))
        
        with open(fn, "w") as logfile:
            logfile.write(body)

        logging.info("Decompress " + fn + " automatically deleted")

        subprocess_run("gzip -d "+fn)

        unzipped = fn.replace(".gz", "")
        print(unzipped)

        logging.info("Split " + unzipped)

        splitted, err, cd = subprocess_run("./split.sh " + unzipped)

        logging.info("Write split to Temp file")

        with open(temp_file, 'w') as tmp:
            tmp.write(splitted)

        out, err, cd = subprocess_run("java -jar JSONCloudtrailMapper-assembly-0.1.jar --mapping-dir %s --map --source %s" % (mappings_folder, temp_file))
      
        post_to_elastic(temp_file, unzipped)

        logging.info("Delete temp files")
        subprocess_run("rm -f " + temp_file )
        subprocess_run("rm -f " + unzipped)
    
       
def bulkify_logs():

    bulk = open(bulk_file_name, 'w')

    for logfile in os.listdir(logs_folder):

        fn = logs_folder+"/"+logfile

        with open(fn, 'rb') as log:
            data = log.read()
            #bulkified = jq(""".Records[] | { index: { _index: "\(.eventSource)", _type: "log" } }, .""").transform(data)
            
            bulkified, err, cd = subprocess_run("./bulkit.sh " + fn)
            bulk.write(bulkified)
    
    bulk.close()

def post_to_elastic(filename, errorFN):
    logging.info("Post bulk to Elasticsearch")
    logging.warn("POST without authentication or SSL certificate")

    data = open(filename, 'rb').read()
    res = requests.post(url='https://'+elastic_host_port+"/_bulk?pretty&refresh",
                    data=data,
                    headers={'Content-Type': 'application/x-ndjson'},
                    proxies=proxyDict,
		            verify=False)

    logging.info("Status Code = " + str(res.status_code))
    logging.debug("Body = " + res.content)

    print res.status_code

    if((res.status_code != 200) or (""""took" : 0, "errors" : true,""" in res.text)):
        logging.error(res.content)
        logging.error("Must retry the file, saved to failed.log")
        
        print("ERROR Must retry the file, saved to failed.log")
        
        with open(failed_file_name, 'a') as failed:
            failed.write(errorFN+"\n")
        #print res.content
    else:
        logging.info("Succesfully posted")
        logging.info("Delete the temp file")
        subprocess_run("rm -f " + filename)
        

if __name__ == '__main__':

    """
    USAGE python Bulker.py --deleteall | --mappings | <yyyy-mm-gg>
    """

    if sys.argv[1] == "--mappings":
        map_all()
    elif sys.argv[1] == "--deleteall":
        delete_all()
    elif sys.argv[1] == "--merge":
        merge_bulks(bulk_folder,"bulk.json")
    else:
        date = sys.argv[1].split("-")

        logging.basicConfig(filename="mylogs/miner_"+sys.argv[1]+".log",
            level=LOG_LEVEL,
            format='[%(levelname)s]:%(asctime)s %(message)s')

        logging.info("Connect to S3 AWS service")

        s3 = boto3.resource('s3', aws_access_key_id = 'AKIAIG27VKXRWNLOY7XQ',
            aws_secret_access_key = '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')
    
        logging.info("Connect to pirelli-cloudtrail bucket")

        bucket = s3.Bucket("pirelli-cloudtrail")

        logging.info("Requested extraction for date="+sys.argv[1])             
        
        if(len(date) == 3):

            logging.info("Start daily extraction...")
            extract_logs(bucket, date[0], date[1], date[2])
        elif(len(date) == 2):

            logging.info("Start monthly extraction...")
            extract_month_logs(bucket, date[0], date[1])
        elif(len(date) == 1):

            logging.info("Start yearly extraction...")
            extract_year_logs(bucket, date[0])
    
        
