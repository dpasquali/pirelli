import requests
import os

server = "vpc-cloudtrail-log-analysis-mw6nxceelz7zgwfhnn6ij7gugq.eu-west-1.es.amazonaws.com"
server_proxy = "10.203.135.168"


os.environ['NO_PROXY'] = '*.pirelli.com, *.es.amazonaws.com'

proxyDict = { 
          'http'  : '', 
          'https' : ''
        }

print("GET CLUSTER HEALTH\n\n")

r = requests.get("https://"+server+"/_cat/health?v", proxies=proxyDict, verify=False)

print(r.content)

print("\nGET INDEX STATUS\n\n")

r = requests.get("https://"+server+"/_cat/indices?v", proxies=proxyDict, verify=False)
print(r.content)

#curl -H 'Content-Type: application/x-ndjson' --proxy http://proxy-web.pirelli.com:80 -XPOST 'https://elasticsearch-log.pirelli.com/_bulk?pretty' --data-binary @bulk_20160104.json



print("\nGET ALLOCATION SPACE\n\n")

r = requests.get("https://"+server+"/_cat/allocation?v", proxies=proxyDict, verify=False)
print(r.content)